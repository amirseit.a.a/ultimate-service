// Package handlers contains the full set of handlers functions and routes
// supported by the web api
package handlers

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/amirseit.a.a/ultimate-service/business/auth"
	"gitlab.com/amirseit.a.a/ultimate-service/business/data/user"
	"gitlab.com/amirseit.a.a/ultimate-service/business/mid"
	"log"
	"net/http"
	"os"

	"gitlab.com/amirseit.a.a/ultimate-service/foundation/web"
)

// API constructs an http.Handler with all application routes defined.
func API(build string,shutdown chan os.Signal,log *log.Logger,a *auth.Auth,db *sqlx.DB) *web.App{
	app := web.NewApp(shutdown,mid.Logger(log),mid.Errors(log),mid.Metrics(),mid.Panics(log))


	cg := checkGroup{
		build: build,
		db: db,
	}

	app.Handle(http.MethodGet,"/readiness",cg.readiness)
	app.Handle(http.MethodGet,"/liveness",cg.liveness)

	ug := userGroup{
		user: user.New(log, db),
		auth: a,
	}
	app.Handle(http.MethodGet, "/users/:page/:rows", ug.query, mid.Authenticate(a), mid.Authorize( auth.RoleAdmin))
	app.Handle(http.MethodGet, "/users/:id", ug.queryByID, mid.Authenticate(a))
	app.Handle(http.MethodGet, "/users/token/:kid", ug.token)
	app.Handle(http.MethodPost, "/users", ug.create, mid.Authenticate(a), mid.Authorize( auth.RoleAdmin))
	app.Handle(http.MethodPut, "/users/:id", ug.update, mid.Authenticate(a), mid.Authorize( auth.RoleAdmin))
	app.Handle(http.MethodDelete, "/users/:id", ug.delete, mid.Authenticate(a), mid.Authorize( auth.RoleAdmin))

	return app
}


