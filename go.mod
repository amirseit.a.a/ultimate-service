module gitlab.com/amirseit.a.a/ultimate-service

go 1.17

require (
	github.com/ardanlabs/conf v1.5.0
	github.com/dgrijalva/jwt-go/v4 v4.0.0-preview1
	github.com/dimfeld/httptreemux/v5 v5.4.0
	github.com/dimiro1/darwin v0.0.0-20191008194338-370f81775d3b
	github.com/go-playground/locales v0.14.0
	github.com/go-playground/universal-translator v0.18.0
	github.com/google/go-cmp v0.3.1
	github.com/google/uuid v1.3.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.4
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20211115234514-b4de73f9ece8
	gopkg.in/go-playground/validator.v9 v9.31.0
)

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
)
